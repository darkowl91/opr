/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package constants;

/**
 *
 * @author Nickolay_Petrash
 */
public class MessageConstants {

    private MessageConstants() {
    }
    public static final String TITLE_KEY = "mainvindow.title";
    public static final String DIALOG_MESSAGE_KEY = "error.dialog.message";
    public static final String DIALOG_CAPTION_KEY = "error.dialog.caption";
    public static final String RB_PATH = "resources.properties.ApplicationResource";
    public static final String CIRCLE_MESSAGE = "А больше ничё не хочешь нажать?";
    public static final String CIRCLE_CAPTION = "Нажми себе";
}
