package run;

import constants.MessageConstants;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javax.swing.JOptionPane;

/**
 *
 * @author Nickolay_Petrash
 */
public class OPR extends Application {

    private static final String MAIN_ICON_PATH = "file:src\\resources\\images\\HeartPNG.png";
    private static final String FXML = "oprFXML.fxml";
    private static final String JAVA_VERSION = "javafx.runtime.version";
    private static final double REQUIRED_VERSION = 2.0;
    private static ResourceBundle bundle;

    @Override
    public void start(Stage stage) throws Exception {
        if (checkVersion()) {
            Locale locale = Locale.getDefault();
            bundle = ResourceBundle.getBundle(MessageConstants.RB_PATH, locale);
            Parent root = FXMLLoader.load(getClass().getResource(FXML), bundle);
            Scene scene = new Scene(root);
            stage.initStyle(StageStyle.DECORATED);
            stage.setTitle(bundle.getString(MessageConstants.TITLE_KEY));
            Image mainIcon = new Image(MAIN_ICON_PATH);
            stage.getIcons().add(mainIcon);
            stage.setScene(scene);
            stage.show();
        } else {
            System.exit(0);
        }
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if (checkVersion()) {
            launch(args);
        } else {
            System.exit(0);
        }
    }

    /**
     *
     * @return java installed version
     */
    private static double getVersion() {
        String version = (String) System.getProperties().get(JAVA_VERSION);
        int pos = 0, count = 0;
        for (; pos < version.length() && count < 1; pos++) {
            if (version.charAt(pos) == '.') {
                count++;
            }
        }
        return Double.parseDouble(version.substring(0, pos));
    }

    /**
     *
     * @return true if version supported else false
     */
    private static boolean checkVersion() {
        if (getVersion() >= REQUIRED_VERSION) {
            return true;
        } else {
            JOptionPane.showMessageDialog(null,
                    bundle.getString(MessageConstants.DIALOG_MESSAGE_KEY),
                    bundle.getString(MessageConstants.DIALOG_CAPTION_KEY),
                    JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
}
