/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Selection;

import java.util.ArrayList;
import model.Chromosome;
import model.Selection;
import model.Sex;
import static org.testng.Assert.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import util.PointNode;

/**
 *
 * @author Soldier
 */
public class Select {
    
    public Select() {
    }
    @Test
            public void range(){
    Chromosome ch1 = new Chromosome("000000000000000000");
    Chromosome ch2 = new Chromosome("101000000000000101");
    Chromosome ch3 = new Chromosome("000000000001010000");
     Chromosome ch4= new Chromosome("000011000000000000");
    Chromosome ch5 = new Chromosome("101100000000000101");
    Chromosome ch6 = new Chromosome("000000000001010110");
     Chromosome ch7= new Chromosome("000100000011010110");
    ch1.setAdaptValue(0.0002);
    ch2.setAdaptValue(0.0004);
    ch3.setAdaptValue(0.0007);
    ch4.setAdaptValue(0.0012);
    ch5.setAdaptValue(0.0104);
    ch6.setAdaptValue(0.0037);
     ch7.setAdaptValue(0.0207);
    ch1.setPoint(new PointNode(2, 10));
   ch2.setPoint(new PointNode(-10, 5));
   ch3.setPoint(new PointNode(0, 5));
    ch4.setPoint(new PointNode(-2, 10));
   ch5.setPoint(new PointNode(-6, 9));
   ch6.setPoint(new PointNode(0, 2));
      ch7.setPoint(new PointNode(2, -10));
    ArrayList<Chromosome> list = new ArrayList<>();
    list.add(ch1);
    list.add(ch2);
    list.add(ch3);
     list.add(ch4);
      list.add(ch5);
       list.add(ch6);
        list.add(ch7);
      Selection select = new Selection();
      select.createRangSelection(7, list);
    }
    
     @Test
            public void roulette(){
    Chromosome ch1 = new Chromosome("011100000000001000");
    Chromosome ch2 = new Chromosome("101000000000000101");
    Chromosome ch3 = new Chromosome("000000000001010000");
     Chromosome ch4= new Chromosome("000011000000000000");
    Chromosome ch5 = new Chromosome("101100000000000101");
    Chromosome ch6 = new Chromosome("000000000001010110");
     Chromosome ch7= new Chromosome("000100000011010110");
    ch1.setAdaptValue(0.0202);
    ch2.setAdaptValue(0.0004);
    ch3.setAdaptValue(0.0007);
    ch4.setAdaptValue(0.0012);
    ch5.setAdaptValue(0.0104);
    ch6.setAdaptValue(0.0037);
     ch7.setAdaptValue(0.0207);
    ch1.setPoint(new PointNode(2, 10));
   ch2.setPoint(new PointNode(-10, 5));
   ch3.setPoint(new PointNode(0, 5));
    ch4.setPoint(new PointNode(-2, 10));
   ch5.setPoint(new PointNode(-6, 9));
   ch6.setPoint(new PointNode(0, 2));
      ch7.setPoint(new PointNode(2, -10));
    ArrayList<Chromosome> list = new ArrayList<>();
    list.add(ch1);
    list.add(ch2);
    list.add(ch3);
     list.add(ch4);
      list.add(ch5);
       list.add(ch6);
        list.add(ch7);
      Selection select = new Selection();
      select.createRulletSelection(7, list);
    }
    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }
}
