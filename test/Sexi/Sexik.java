/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Sexi;

import java.util.ArrayList;
import model.Chromosome;
import model.Sex;
import static org.testng.Assert.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import util.PointNode;

/**
 *
 * @author Soldier
 */
public class Sexik {
    
    public Sexik() {
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
     public void better() {
    Chromosome ch1 = new Chromosome("000000000000000000");
    Chromosome ch2 = new Chromosome("101000000000000101");
    Chromosome ch3 = new Chromosome("000000000001010000");
    ch1.setAdaptValue(0.0002);
    ch2.setAdaptValue(0.0004);
    ch3.setAdaptValue(0.0007);
    ArrayList<Chromosome> list = new ArrayList<>();
    list.add(ch1);
    list.add(ch2);
    list.add(ch3);
      Sex s = new Sex();
      s.findBetterWorsePair(list);
    
    }
    
    @Test
    public void mindist(){
    Chromosome ch1 = new Chromosome("000000000000000000");
    Chromosome ch2 = new Chromosome("101000000000000101");
    Chromosome ch3 = new Chromosome("000000000001010000");
    ch1.setAdaptValue(0.0002);
    ch2.setAdaptValue(0.0004);
    ch3.setAdaptValue(0.0007);
    ch1.setPoint(new PointNode(2, 10));
   ch2.setPoint(new PointNode(-10, 5));
   ch3.setPoint(new PointNode(0, 5));
    
    ArrayList<Chromosome> list = new ArrayList<>();
    list.add(ch1);
    list.add(ch2);
    list.add(ch3);
      Sex s = new Sex();
      s.findMinDistancePair(list);
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }
}
